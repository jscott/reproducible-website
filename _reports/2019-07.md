---
layout: report
year: "2019"
month: "07"
month_name: "July"
title: "Reproducible builds in July 2019"
draft: true
---

#### Upstream patches

* Bernhard M. Wiedemann:
    * [calibre](https://github.com/kovidgoyal/calibre/pull/1014) (sort python glob)
    * [python-futurist](https://review.opendev.org/669130) (drop environment.pickle)
    * [herbstluftwm](https://github.com/herbstluftwm/herbstluftwm/pull/563) (use cmake TIMESTAMP)
    * [obs-build](https://github.com/openSUSE/obs-build/pull/510) (use gzip -n in Debian package build)
    * [python-geolib](https://build.opensuse.org/request/show/713240) (drop environment.pickle)
    * [perl-File-Unpack](https://build.opensuse.org/request/show/713417) (fix parallelism race ; [submitted upstream](https://github.com/jnweiger/perl-File-Unpack/pull/9) ; likely orphaned)
    * [python-slycot](https://build.opensuse.org/request/show/713579) (drop unreproducible .pyc files)
    * [lbreakout2](https://build.opensuse.org/request/show/714451) (strip png date+time)
    * [notpacman](https://build.opensuse.org/request/show/714453) (strip png date+time)
    * [frogatto](https://build.opensuse.org/request/show/714463) (strip png date+time)
    * [bubbros](https://build.opensuse.org/request/show/714457) (strip png date+time ; also [filed upstream](https://sourceforge.net/p/bub-n-bros/patches/3/))
    * [HSAIL-Tools](https://build.opensuse.org/request/show/714387) (sort hash ; already [submitted upstream](https://github.com/HSAFoundation/HSAIL-Tools/pull/54))
    * [MozillaFirefox](https://build.opensuse.org/request/show/714438) + [MozillaThunderbird](https://build.opensuse.org/request/show/714441) (fix [race bug](https://bugzilla.opensuse.org/show_bug.cgi?id=1137970))
    * [boswars](https://build.opensuse.org/request/show/714579) (sort python readdir ; also [submitted upstream](https://savannah.nongnu.org/patch/index.php?9830)
    * [netpanzer](https://build.opensuse.org/request/show/714585) (sort scons glob/readdir)
    * [sienna](https://build.opensuse.org/request/show/714584) (zip -X)
    * [duckmarines](https://build.opensuse.org/request/show/714601) (zip -X)
    * [wordwarvi](https://build.opensuse.org/request/show/714611) (date ; [already upstream](https://github.com/smcameron/wordwarvi/commit/c890eb38211741261f0e18692131ebfcddc847e8))
    * [yadex](https://build.opensuse.org/request/show/714615) (date/mtime ; not upstream)
    * [worldofpadman](https://build.opensuse.org/request/show/714623) (date ; also [merged in upstream ioquake3](https://github.com/ioquake/ioq3/pull/414))
    * [python-nautilus](https://build.opensuse.org/request/show/714880) (python date ; [already upstream](https://github.com/GNOME/nautilus-python/pull/6))
    * [znc](https://build.opensuse.org/request/show/714939) (avoid parallelism race from CMakeFiles)
    * [springrts](https://build.opensuse.org/request/show/715002) (strip-nondetermism zip mtime)
    * enabling Link Time Optimization (LTO) in openSUSE Tumbleweed caused issues, not because of LTO itself, but because openSUSE adds it to CFLAGS with the number of cores:
        * [debuginfo/rpm header](https://bugzilla.opensuse.org/show_bug.cgi?id=1140896) (gcc -flto debuginfo/rpm OPTFLAGS parallelism)
        * [CFLAGS in binaries](https://bugzilla.opensuse.org/show_bug.cgi?id=1141323) (packages embed CFLAGS with -flto=$N : fldigi gmp haproxy ImageMagick lyx neovim tboot tcl znc)
    * [colobot-data](https://github.com/colobot/colobot-data/pull/41) (sort python readdir)
    * [griefly](https://github.com/griefly/griefly/pull/508) (sort python readdir)
    * [vdrift](https://github.com/VDrift/vdrift/pull/168) (merged ; date in scons/python)
    * [blockattack](https://github.com/blockattack/blockattack-game/pull/18) (merged ; sort zip shell call + gzip -n)


* Neal Gompa / Michael Schröder / Miro Hrončok:
    * Fedora's recent [rpm-config change](https://src.fedoraproject.org/rpms/redhat-rpm-config/pull-request/57) prompted some new changes:
        * [rpm](https://github.com/rpm-software-management/rpm/pull/785) (make unreproducible Build Date the default - solution: `%use_source_date_epoch_as_buildtime Y`)
        * [cpython](https://github.com/fedora-python/cpython/pull/3) (revert to unreproducible time-based .pyc files)

* kpcyrd: [](https://github.com/alpinelinux/abuild/pull/93)

#### FIXME

* [FIXME](https://github.com/bmwiedemann/theunreproduciblepackage/commit/e5d59a3dda050b5c52b59af0ab610936d037c3b2) floating point

* [FIXME](http://en.alessiotreglia.com/articles/cosmos-hub-and-reproducible-builds/)

* [FIXME](https://lists.debian.org/<20190707014700.GF15255@powdarrmonkey.net>):

* [FIXME](https://glandium.org/blog/?p=3923)

* [FIXME](https://salsa.debian.org/reproducible-builds/transparency)

* [FIXME](https://debconf19.debconf.org/talks/66-software-transparency-improving-package-manager-security/)

>   binary maintainer uploads for bullseye
>   =========================================
>
>   The release of buster also means the bullseye release cycle is about to begin.
>   From now on, we will no longer allow binaries uploaded by maintainers to
>   migrate to testing. This means that you will need to do source-only uploads if
>   you want them to reach bullseye.

* [FIXME](https://bugs.debian.org/926242#132)

* [FIXME](https://github.com/jgalenson/reproducible-rustc) Joel Galenson made progress on a reproducible rustc including `--remap-path-prefix`

* [FIXME](https://bugs.debian.org/932849): ftp.debian.org: NEW process looses .buildinfo files

* David Bremner posted his blog post about [Yet another buildinfo database](https://www.cs.unb.ca/~bremner//blog/posts/builtin-pho/), written at DebConf19.

#### Media coverage

* [Nico Alt's article from C'T about Reproducible F-Droid](https://nico.dorfbrunnen.eu/posts/2019/reproducibility-fdroid/), [originally post](https://www.heise.de/select/ct/2019/14/1561892042086279)



* [FIXME](https://github.com/zephyrproject-rtos/zephyr/pull/17494)

* "Reprotest needs a maintainer"

* [FIXME](https://debconf19.debconf.org/talks/30-reproducible-builds-aiming-for-bullseye/)

* [FIXME](https://debconf19.debconf.org/talks/116-software-transparency-bof/)

* [F-droid reproducibility etc mentioned on Late Night Linux podcast from 14m12s](https://pca.st/D630#t=849)

* [Molior - Debian Build System mentions 'reproducible builds'](https://github.com/molior-dbs/molior)

* ["Yet another buildinfo database."](http://www.cs.unb.ca/~bremner//blog/posts/builtin-pho/)

* [#932849 ftp.debian.org: 'dak process-policy new' loses .buildinfo files](http://bugs.debian.org/932849)
